﻿using System.ComponentModel.DataAnnotations;

namespace Wwg.KeyValue.Api.Models
{
    public class KeyValueApiModel
    {
        [Required]
        [MaxLength(1024, ErrorMessage = "{0} cannot be more than 1024 characters long.")]
        public string Value { get; set; }
    }
}
