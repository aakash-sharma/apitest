﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using Wwg.KeyValue.Api.Attributes;
using Wwg.KeyValue.Api.Models;
using Wwg.KeyValue.Api.Service;

namespace Wwg.KeyValue.Api.Controllers
{
    [ApiController]
    [Route("api/keys")]
    [ModelStateValidation]
    public class KeyValueController : ControllerBase
    {
        private readonly IKeyValueService _keyValueService;
        private const string KeyValidationMessage = "Key must be less than 32 characters, alphanumeric and url-safe(only alphanumeric, hyphen, period, underscore, tilde)";

        public KeyValueController(IKeyValueService keyValueService)
        {
            _keyValueService = keyValueService;
        }

        /// <summary>
        /// Returns value by key.
        /// </summary>
        /// <response code="200">The value has been returned.</response>
        /// <response code="400">The input was incorrect.</response>
        /// <response code="404">The key was not found.</response>
        [HttpGet]
        [Route("{key}", Name = nameof(GetValue))]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetValue([Required] string key)
        {
            if(string.IsNullOrWhiteSpace(key))
                return BadRequest("Invalid key");

            try
            {
                var (value, status) = _keyValueService.GetValue(key);

                if (!status)
                    return NotFound("The key does not exist.");

                return Ok(value);
            }
            catch(ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Adds a new key value pair.
        /// </summary>
        /// <response code="201">The value has been added.</response>
        /// <response code="400">The input was incorrect.</response>
        [HttpPost]
        [Route("{key}")]
        [ProducesResponseType(typeof(string), 201)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Add(string key, [FromBody] [Required] KeyValueApiModel keyValueApiModel)
        {
            if(keyValueApiModel is null)
                return BadRequest("Missing model");

            if (!_keyValueService.IsKeyValid(key))
                return BadRequest(KeyValidationMessage);

            try
            {
                var status = _keyValueService.TryAdd(key, keyValueApiModel.Value);

                if (!status)
                    return BadRequest("An item with the same key already exists.");

                return CreatedAtAction(nameof(GetValue), new { key }, key);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Updates value for existing key.
        /// </summary>
        /// <response code="200">The value has been updated.</response>
        /// <response code="400">The input was incorrect.</response>
        [HttpPut]
        [Route("{key}")]
        [ProducesResponseType(typeof(string), 201)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Update(string key, [FromBody][Required] KeyValueApiModel keyValueApiModel)
        {
            if (keyValueApiModel is null)
                return BadRequest("Missing model");

            if (!_keyValueService.IsKeyValid(key))
                return BadRequest(KeyValidationMessage);

            try
            {
                var status = _keyValueService.TryUpdate(key, keyValueApiModel.Value);

                if (!status)
                    return BadRequest("An item with the given key does not exist.");

                return Ok(key);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
