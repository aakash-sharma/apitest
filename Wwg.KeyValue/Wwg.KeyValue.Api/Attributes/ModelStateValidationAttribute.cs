﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wwg.KeyValue.Api.Attributes
{
    public class ModelStateValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var validationErrors = context.ModelState.Values.Where(m => m.ValidationState == ModelValidationState.Invalid)
                    .SelectMany(m => m.Errors.Select(e => (null == e.Exception) ? e.ErrorMessage : e.Exception.Message), (modelState, errorMessage) =>  errorMessage)
                    .ToArray();
                context.Result = new BadRequestObjectResult(string.Join(";", validationErrors));
            }
        }
    }
}
