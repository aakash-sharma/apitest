﻿using FluentAssertions;
using Wwg.KeyValue.Api.Models;
using Xunit;

namespace Wwg.KeyValue.Api.Test.Api.Model
{
    public class KeyValueApiModelTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("   ")]
        public void Validate_KeyValue_Model_When_No_Value(string value)
        {
            // ARRANGE
            var model = new KeyValueApiModel
            {
                Value = value
            };

            // ACT
            var results = ModelValidationTestHelper.Validate(model);

            // ASSERT
            results.Count.Should().Be(1);
            results[0].ErrorMessage.Should().BeEquivalentTo("The Value field is required.");
        }

        [Fact]
        public void Validate_KeyValue_Model_When_Valid_Value()
        {
            // ARRANGE
            var model = new KeyValueApiModel
            {
                Value = KeyValueTestFixture.FakeValue
            };

            // ACT
            var results = ModelValidationTestHelper.Validate(model);

            // ASSERT
            results.Count.Should().Be(0);
        }

        [Fact]
        public void Validate_KeyValue_Model_When_Key_Is_Longer_Than_1024()
        {
            // ARRANGE
            var model = new KeyValueApiModel
            {
                Value = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it
                        to make a type specimen book. It has survived not only five centuries, 
                        but also the leap into electronic typesetting, remaining essentially unchanged. 
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        It is a long established fact that a reader will be distracted by the readable content of a page when 
                        looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, 
                        as opposed to using 'Content here, content here', making it look like readable English."
            };

            // ACT
            var results = ModelValidationTestHelper.Validate(model);

            // ASSERT
            results.Count.Should().Be(1);
            results[0].ErrorMessage.Should().BeEquivalentTo("Value cannot be more than 1024 characters long.");
        }
    }
}
