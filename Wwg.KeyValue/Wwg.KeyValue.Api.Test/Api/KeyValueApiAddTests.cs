using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Moq;
using Wwg.KeyValue.Api.Models;

namespace Wwg.KeyValue.Api.Test.Api
{
    public class KeyValueApiAddTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValueApiAddTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void When_Missing_Model_Add_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController();

            var response = controller.Add(KeyValueTestFixture.FakeKey, null);
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Invalid_Model_Add_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController();

            var response = controller.Add(KeyValueTestFixture.FakeKey, new KeyValueApiModel());
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Invalid_Key_Add_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(false);

            var response = controller.Add(KeyValueTestFixture.FakeKey, new KeyValueApiModel());
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Key_Already_Exists_Add_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(true);
            mockService.Setup(mock => mock.TryAdd(It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            var response = controller.Add(KeyValueTestFixture.FakeKey, new KeyValueApiModel { Value = KeyValueTestFixture.FakeValue });
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            result.Value.Should().Be("An item with the same key already exists.");
        }

        [Fact]
        public void When_New_Key_Add_Should_Return_Http_Created()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(true);
            mockService.Setup(mock => mock.TryAdd(KeyValueTestFixture.FakeKey, It.IsAny<string>())).Returns(true);

            var response = controller.Add(KeyValueTestFixture.FakeKey, new KeyValueApiModel { Value = KeyValueTestFixture.FakeValue });
            response.Should().NotBeNull();
            var result = response as CreatedAtActionResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.Created);
            result.Value.Should().Be(KeyValueTestFixture.FakeKey);
        }
    }
}
