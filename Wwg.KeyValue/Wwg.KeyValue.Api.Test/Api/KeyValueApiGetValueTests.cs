using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Moq;

namespace Wwg.KeyValue.Api.Test.Api
{
    public class KeyValueApiGetValueTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValueApiGetValueTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void When_Missing_Key_GetValue_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController();

            var response = controller.GetValue(null);
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_ITem_With_Given_Key_Does_Not_Exist_GetValue_Should_Return_NotFound()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.GetValue(It.IsAny<string>())).Returns((null, false));

            var response = controller.GetValue(KeyValueTestFixture.FakeKey);
            response.Should().NotBeNull();
            var result = response as NotFoundObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void When_New_Key_Add_Should_Return_Http_Ok()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.GetValue(KeyValueTestFixture.FakeKey)).Returns((KeyValueTestFixture.FakeValue, true));

            var response = controller.GetValue(KeyValueTestFixture.FakeKey);
            response.Should().NotBeNull();
            var result = response as OkObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
            result.Value.Should().Be(KeyValueTestFixture.FakeValue);
        }
    }
}
