using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Moq;
using Wwg.KeyValue.Api.Models;

namespace Wwg.KeyValue.Api.Test.Api
{
    public class KeyValueApiUpdateTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValueApiUpdateTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void When_Missing_Model_Update_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController();

            var response = controller.Update(KeyValueTestFixture.FakeKey, null);
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Invalid_Model_Update_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController();

            var response = controller.Update(KeyValueTestFixture.FakeKey, new Models.KeyValueApiModel());
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Invalid_Key_Update_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(false);

            var response = controller.Update(KeyValueTestFixture.FakeKey, new KeyValueApiModel());
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void When_Key_Does_Not_Exist_Update_Should_Return_Bad_Request()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(true);
            mockService.Setup(mock => mock.TryUpdate(It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            var response = controller.Update(KeyValueTestFixture.FakeKey, new KeyValueApiModel { Value = KeyValueTestFixture.FakeValue });
            response.Should().NotBeNull();
            var result = response as BadRequestObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            result.Value.Should().Be("An item with the given key does not exist.");
        }

        [Fact]
        public void When_New_Key_Update_Should_Return_Http_Ok()
        {
            var controller = _fixture.CreateKeyValueController(out var mockService);
            mockService.Setup(mock => mock.IsKeyValid(It.IsAny<string>())).Returns(true);
            mockService.Setup(mock => mock.TryUpdate(KeyValueTestFixture.FakeKey, It.IsAny<string>())).Returns(true);

            var response = controller.Update(KeyValueTestFixture.FakeKey, new KeyValueApiModel { Value = KeyValueTestFixture.FakeValue });
            response.Should().NotBeNull();
            var result = response as OkObjectResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
            result.Value.Should().Be(KeyValueTestFixture.FakeKey);
        }
    }
}
