﻿using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Wwg.KeyValue.Api.Controllers;
using Wwg.KeyValue.Api.Service;

namespace Wwg.KeyValue.Api.Test
{
    public class KeyValueTestFixture
    {
        public static string FakeKey => "fakekey";
        public static string FakeValue => "fakevalue";

        public IKeyValueService CreateKeyValueService()
        {
            return new KeyValueService(new NullLogger<KeyValueService>());
        }

        public KeyValueController CreateKeyValueController()
        {
            return CreateKeyValueController(out _);
        }

        public KeyValueController CreateKeyValueController(out Mock<IKeyValueService> mockKeyValueService)
        {
            mockKeyValueService = new Mock<IKeyValueService>();

            return new KeyValueController(mockKeyValueService.Object);
        }
    }
}
