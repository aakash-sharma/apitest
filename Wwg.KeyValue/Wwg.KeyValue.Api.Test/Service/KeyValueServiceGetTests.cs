using System;
using Xunit;
using FluentAssertions;

namespace Wwg.KeyValue.Api.Test.Service
{
    public class KeyValueServiceGetTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValueServiceGetTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        public void When_Missing_Key_GetValue_Throws_Exception(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            Func<(string, bool)> func = () => service.GetValue(key);
            func.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void When_Key_Does_Not_Exist_GetValue_Returns_Null_And_False()
        {
            using var service = _fixture.CreateKeyValueService();

            var (result, status) = service.GetValue(KeyValueTestFixture.FakeKey);
            result.Should().BeNull();
            status.Should().BeFalse();
        }

        [Fact]
        public void When_Key_Exists_GetValue_Returns_Value_And_True()
        {
            using var service = _fixture.CreateKeyValueService();

            // add key 
            service.TryAdd(KeyValueTestFixture.FakeKey, KeyValueTestFixture.FakeValue);

            // get key
            var (result, status) = service.GetValue(KeyValueTestFixture.FakeKey);
            result.Should().Be(KeyValueTestFixture.FakeValue);
            status.Should().BeTrue();
        }

        [Theory]
        [InlineData("TESTKEY")]
        [InlineData("testKEY")]
        [InlineData("TeStKeY")]
        public void When_Key_Exists_In_Different_GetValue_Returns_Value_And_True(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            // add key 
            service.TryAdd("testkey", KeyValueTestFixture.FakeValue);

            // get key
            var (result, status) = service.GetValue(key);
            result.Should().Be(KeyValueTestFixture.FakeValue);
            status.Should().BeTrue();
        }

        [Theory]
        [InlineData("key1", "")]
        [InlineData("key2", null)]
        public void When_Key_Exists_With_Value_As_Null_Or_Empty_GetValue_Returns_Value_And_True(string key, string value)
        {
            using var service = _fixture.CreateKeyValueService();

            // add key 
            service.TryAdd(key, value);

            // get key
            var (result, status) = service.GetValue(key);
            result.Should().BeNullOrEmpty();
            status.Should().BeTrue();
        }
    }
}
