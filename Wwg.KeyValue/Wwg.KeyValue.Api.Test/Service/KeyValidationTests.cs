﻿using System;
using Xunit;
using FluentAssertions;

namespace Wwg.KeyValue.Api.Test.Service
{
    public class KeyValidationTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValidationTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        public void When_Missing_Key_IsKeyValid_Throws_Exception(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            Func<bool> func = () => service.IsKeyValid(key);
            func.Should().Throw<ArgumentNullException>();
        }

        [Theory]
        [InlineData("valid")]
        [InlineData("valid-1")]
        [InlineData("valid~1")]
        [InlineData("valid.1")]
        [InlineData("valid_1")]
        public void When_Valid_Key_IsKeyValid_Returns_True(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            var status = service.IsKeyValid(key);
            status.Should().BeTrue();
        }

        [Fact]
        public void When_Key_Is_Longer_Than_32_IsKeyValid_Returns_False()
        {
            using var service = _fixture.CreateKeyValueService();

            var status = service.IsKeyValid("key-value-grater-than-thirty-two-characters");
            status.Should().BeFalse();
        }

        [Theory]
        [InlineData("!")]
        [InlineData("@")]
        [InlineData("=")]
        [InlineData("<")]
        [InlineData(">")]
        [InlineData("a s")]
        public void When_InvalidCharacters_IsKeyValid_Returns_False(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            var status = service.IsKeyValid(key);
            status.Should().BeFalse();
        }
    }
}
