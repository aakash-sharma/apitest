using System;
using Xunit;
using FluentAssertions;

namespace Wwg.KeyValue.Api.Test.Service
{
    public class KeyValueServiceAddTests : IClassFixture<KeyValueTestFixture>
    {
        private readonly KeyValueTestFixture _fixture;

        public KeyValueServiceAddTests(KeyValueTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData(null)]
        public void When_Missing_Key_TryAdd_Throws_Exception(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            Func<bool> func = () => service.TryAdd(key, KeyValueTestFixture.FakeValue);
            func.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void When_Key_Does_Not_Exist_TryAdd_Returns_True()
        {
            using var service = _fixture.CreateKeyValueService();

            var result = service.TryAdd(KeyValueTestFixture.FakeKey, KeyValueTestFixture.FakeValue);
            result.Should().BeTrue();
        }

        [Fact]
        public void When_Key_Already_Exists_TryAdd_Returns_False()
        {
            using var service = _fixture.CreateKeyValueService();

            // add key 
            service.TryAdd(KeyValueTestFixture.FakeKey, KeyValueTestFixture.FakeValue);

            // add same key again
            var result = service.TryAdd(KeyValueTestFixture.FakeKey, KeyValueTestFixture.FakeValue);
            result.Should().BeFalse();
        }

        [Theory]
        [InlineData("TESTKEY")]
        [InlineData("testKEY")]
        [InlineData("TeStKeY")]
        public void When_Key_Already_Exists_In_Different_Case_TryAdd_Returns_False(string key)
        {
            using var service = _fixture.CreateKeyValueService();

            // add key 
            service.TryAdd("testkey", KeyValueTestFixture.FakeValue);

            // add same key again
            var result = service.TryAdd(key, KeyValueTestFixture.FakeValue);
            result.Should().BeFalse();
        }
    }
}
