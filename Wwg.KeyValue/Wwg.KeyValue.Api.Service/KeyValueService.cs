﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Wwg.KeyValue.Api.Service
{
    /// <summary>
    /// Implements the key value service
    /// </summary>
    /// <inheritdoc />
    public class KeyValueService : IKeyValueService
    {
        private static Dictionary<string, string> _keyValues;
        private readonly ILogger<KeyValueService> _logger;
        private bool _disposed = false;

        public KeyValueService(ILogger<KeyValueService> logger)
        {
            _keyValues = new Dictionary<string, string>();
            _logger = logger;
        }

        public (string value, bool status) GetValue(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                _logger.LogError("Key is null or empty.");
                throw new ArgumentNullException(nameof(key));
            }

            key = key.ToLower();

            // if key does not exist then return null value with status as false.
            if (!_keyValues.ContainsKey(key))
            {
                _logger.LogWarning("The Key {Key} does not exist.", key);
                return (null, false);
            }

            // if key exists then return value with status as true. An empty string or null are valid values.
            return (_keyValues[key], true);
        }

        public bool TryAdd(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                _logger.LogError("Key is null or empty.");
                throw new ArgumentNullException(nameof(key));
            }

            key = key.ToLower();

            if (_keyValues.ContainsKey(key))
            {
                _logger.LogError("The Key {Key} already exists.", key);
                return false;
            }

            try
            {
                // an empty value is allowed.
                _keyValues.Add(key, value);
            }
            catch(ArgumentException aex) 
            {
                // even though we check for key's existence before, this ensures we capture the exception when concurrent requests are recieved with same key.
                _logger.LogError("An error occured while saving the {Key}. Error is {@Error}", key, aex);
                return false;
            }

            return true;
        }

        public bool TryUpdate(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                _logger.LogError("Key is null or empty.");
                throw new ArgumentNullException(nameof(key));
            }

            key = key.ToLower();

            if (!_keyValues.ContainsKey(key))
            {
                _logger.LogError("The Key {Key} does not exist.", key);
                return false;
            }

            // an empty value is allowed.
            _keyValues[key] = value;

            return true;
        }

        public bool IsKeyValid(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException(nameof(key));

            if (key.Length > 32)
                return false;

            var regex = @"^[a-zA-Z0-9.~\-_]+$";
            return Regex.IsMatch(key, regex);
        }

        // This class is made disposable as unit tests were not playing well running in parallel without delete/clear method in this class.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_keyValues != null)
                    {
                        _keyValues = null;
                    }
                }

                _disposed = true;
            }
        }
    }
}
