﻿
using System;

namespace Wwg.KeyValue.Api.Service
{
    /// <summary>
    /// Defines the key value service.
    /// </summary>
    public interface IKeyValueService : IDisposable
    {
        /// <summary>
        /// Gets value by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>A tuple of value and status. 
        /// If key does not exist status is false. 
        /// A key can have null or empty stirng as vlaid values.
        /// </returns>
        (string value, bool status) GetValue(string key);

        /// <summary>
        /// Adds a key value pair if it key does not already exist.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns>A boolean stating whether add was successful.</returns>
        bool TryAdd(string key, string value);

        /// <summary>
        /// Updates a value of the item for the given key
        /// </summary>
        /// <param name="key">The key of the item to be updated.</param>
        /// <param name="value">The new value.</param>
        /// <returns>A boolean stating whether update was successful.</returns>
        bool TryUpdate(string key, string value);

        /// <summary>
        /// Validates the key.
        /// </summary>
        /// <param name="key">The key to validate.</param>
        /// <returns>Validation result.</returns>
        bool IsKeyValid(string key);
    }
}
